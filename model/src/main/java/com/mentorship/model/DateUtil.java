package com.mentorship.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Maks on 12.11.2016.
 */
public class DateUtil {

    public static Date parseDate(String textdate){
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        try {
            date = simpleDateFormat.parse(textdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  date;
    }
}
