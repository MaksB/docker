package com.mentorship.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Created by Maks on 12.11.2016.
 */

@Data
@XmlRootElement(name = "catalog")
public class Catalog {

    private String title;
    private Collection<Book> booksList;

    public String getTitle() {
        return title;
    }

    @XmlElement(name = "title")
    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElementWrapper(name = "booksList")
    @XmlElement(name = "book")
    public Collection<Book> getBooksList() {
        return booksList;
    }


    public void setBooksList(Collection<Book> booksList) {
        this.booksList = booksList;
    }




}
