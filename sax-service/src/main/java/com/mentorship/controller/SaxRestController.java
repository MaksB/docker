package com.mentorship.controller;

import com.mentorship.model.Catalog;
import com.mentorship.parser.BookSAXParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Maks on 13.11.2016.
 */
@RestController
@RequestMapping(value = "/sax")
public class SaxRestController {

    @Autowired
    private BookSAXParser bookSAXParser;

    @GetMapping(value = "/catalog")
    public Catalog getCatalog(){
        return bookSAXParser.parseCatalogXml();
    }

    @GetMapping("/books/price")
    public String getBooksByPrice() {
        return bookSAXParser.getBooksByPrice();
    }

}
